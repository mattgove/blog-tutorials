#!/usr/bin/env python
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np

# Make a map zoomed in on the southwestern US
parallels_color = "#777777"
m = Basemap(projection='stere',lon_0=-110,lat_0=90.,lat_ts=0,\
            llcrnrlat=20,urcrnrlat=40,\
            llcrnrlon=-120,urcrnrlon=-90,\
            rsphere=6371200.,resolution='l',area_thresh=10000)
# draw coastlines.
m.shadedrelief()
m.drawcoastlines()
m.drawcountries()
m.drawstates()

# meridians on bottom and left
parallels = np.arange(0.,81,10.)
# labels format is [left,right,top,bottom]
m.drawparallels(parallels,labels=[False,True,True,False], color=parallels_color)
meridians = np.arange(10.,351.,10.)
m.drawmeridians(meridians,labels=[True,False,False,True], color=parallels_color)

plt.title("A Simple Map of the Southwestern US Terrain")
plt.savefig('sw-usa.png', dpi=200)
plt.show()
