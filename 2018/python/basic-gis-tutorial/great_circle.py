#!/usr/bin/env python
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np

# Great Circle Route
class City(object):

    def __init__(self, lat, lon, name):
        self.latitude = lat
        self.longitude = lon
        self.name = name

def city_file_name(city):
    return city.replace(' ', '-').lower()

new_york = City(40.78, -73.98, "New York")
los_angeles = City(34.052, -118.244, "Los Angeles")
paris = City(48.857, 2.352, "Paris")
tokyo = City(35.69, 139.692, "Tokyo")
bangkok = City(13.756, 100.502, "Bangkok")
cape_town = City(-33.925, 18.424, "Cape Town")

# Set Origin and Destination
origin = los_angeles
destination = paris

lat_0 = (origin.latitude + destination.latitude) / 2
lon_0 = (origin.longitude + destination.longitude) / 2
title = "Great Circle from {} to {}".format(origin.name, destination.name)
lat_lon_adjuster = 10

# Calculate max and min lats/longs
min_lat = origin.latitude if origin.latitude < destination.latitude else destination.latitude
min_lon = origin.longitude if origin.longitude < destination.longitude else destination.longitude
max_lat = origin.latitude if origin.latitude > destination.latitude else destination.latitude
max_lon = origin.longitude if origin.longitude > destination.longitude else destination.longitude

min_lat -= lat_lon_adjuster
min_lon -= lat_lon_adjuster
max_lat += lat_lon_adjuster
max_lon += lat_lon_adjuster

m = Basemap(projection='ortho', lat_0=lat_0, lon_0=lon_0, resolution='l', area_thresh=1000.0)
# m = Basemap(llcrnrlon=min_lon, llcrnrlat=min_lat, urcrnrlon=max_lon, urcrnrlat=max_lat,\
#             rsphere=(6378137.00,6356752.3142),\
#             resolution='l',projection='merc',\
#             lat_0=lat_0,lon_0=lon_0,lat_ts=20.)
m.shadedrelief()
m.drawcountries()
m.drawstates()
m.drawgreatcircle(
    origin.longitude,
    origin.latitude,
    destination.longitude,
    destination.latitude
)

plot_filename = "great-circle-{}-to-{}-globe.png".format(
    city_file_name(origin.name),
    city_file_name(destination.name)
)
plt.title(title)
plt.savefig(plot_filename, dpi=200)
plt.show()
