#!/usr/bin/env python
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np

m = Basemap(projection='ortho', lat_0=50, lon_0=-100, resolution='l', area_thresh=1000.0)
m.drawcoastlines()
m.drawcountries()
plt.savefig('basic-map.png', dpi=200)
plt.show()
