#!/usr/bin/env python
import netCDF4
import os
import datetime
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap, shiftgrid, addcyclic


# Define our plot's parameters/settings
plot_variable = 'wind'              # Variable to plot: temperature, wind, or pressure (default)
date_string = '2012081112'          # yyyyddmmhh
level_option = 50000                # Pascals; use -1 for sea level pressure
map_projection = 'lcc'              # Use 'lcc' or 'ortho' for now
plot_contours = True                # Plot contour lines
output_filename = 'netcdf_example'  # Will save as png with this name
netcdf_directory = os.getcwd()      # Directory containing the NetCDF file
plot_barbs = True if plot_variable == 'wind' else False


dt = datetime.datetime.strptime(date_string, '%Y%m%d%H')
netcdf_filename = 'gfs_4_{}_{}00_000.nc'.format(
    dt.strftime('%Y%m%d'),
    dt.strftime('%H')
)
netcdf_filepath = os.path.join(netcdf_directory, netcdf_filename)

# Define our variables
f = netCDF4.Dataset(netcdf_filepath, 'r')
lons = f.variables['lon_0'][:]
lats = f.variables['lat_0'][::-1]   # Read in reverse direction
levels = f.variables['lv_ISBL0'][:]
# print levels  # Uncomment this line to see your options for height data, in Pascals
plot_level = 100000 if level_option == -1 else level_option
level_index = np.ravel(levels==plot_level)

if level_option == -1:
    pressure = f.variables['PRMSL_P0_L101_GLL0'][::-1,:]/100
else:
    pressure = f.variables['HGT_P0_L100_GLL0'][level_index,::-1,:].squeeze()

# Define variables for temperature and wind vectors (u and v)
temperature = f.variables['TMP_P0_L100_GLL0'][level_index,::-1,:].squeeze()
u = f.variables['UGRD_P0_L100_GLL0'][level_index,::-1,:].squeeze()
v = f.variables['VGRD_P0_L100_GLL0'][level_index,::-1,:].squeeze()

f.close()

# Unit conversions: temperature => Celsius, Wind => knots, Pressure => hPa
temperature = temperature - 273.15
u = u * 1.94384
v = v * 1.94384
level_hpa = level_option / 100

lons_ = lons    # Dummy variable for calculations
pressure, lons = addcyclic(pressure, lons_)
temperature, lons = addcyclic(temperature, lons_)
u, lons = addcyclic(u, lons_)
v, lons = addcyclic(v, lons_)

# Refresh Dimensions
x, y = np.meshgrid(lons, lats)

# Set global figure properties
golden = (np.sqrt(5) + 1.)/2

# Set contour levels and intervals (in hPa)
if level_option == -1:
    base_contour = 1012
    contour_interval = 4
elif level_option == 50000:
    base_contour = 5580
    contour_interval = 60
elif level_option == 30000:
    base_contour = 9180
    contour_interval = 60
else:
    base_contour = 1440   # This probably needs to be adjusted for certain levels
    contour_interval = 30

min_contour = base_contour - 20 * contour_interval
max_contour = base_contour + 20 * contour_interval
pressure_contours = np.arange(min_contour, max_contour+1, contour_interval)

# Set temperature contour from -80 to 50 C with intervals of 2 C
temperature_contours = np.arange(-80, 51, 2)

# Set wind contours
wind_contours = np.arange(0, 101, 10)
shade_contours = np.arange(70, 101, 10)

# Set units for the title
if plot_variable == 'wind':
    variable_units = 'kts'
elif plot_variable == 'temperature':
    variable_units = 'C'
else:
    variable_units = 'hPa'

# Set title text for figure
if plot_variable == 'pressure' and level_option > 0:
    variable_title = 'Geopotential Height'
    variable_units = 'm'
else:
    variable_title = plot_variable.title()

if level_option == -1:
    level_title = 'Surface'
else:
    level_title = "{} hPa".format(level_hpa)

title_date = dt.strftime('%d %b %Y')
title_time = dt.strftime('%H00')

figure_title = '{} {} ({}) Valid {} at {} UTC'.format(
    level_title,
    variable_title,
    variable_units,
    title_date,
    title_time
)

# Create figure and draw map
fig = plt.figure(figsize=(8., 16./golden), dpi=128)
ax1 = fig.add_axes([0.1, 0.1, 0.8, 0.8])

if map_projection == 'ortho':
    m = Basemap(projection='ortho', lat_0=50, lon_0=260,
                resolution='l', area_thresh=1000., ax=ax1)
elif map_projection == 'lcc':
    m = Basemap(llcrnrlon=-125.5,llcrnrlat=15.,urcrnrlon=-30.,urcrnrlat=50.352,
            rsphere=(6378137.00,6356752.3142),
            resolution='l',area_thresh=1000.,projection='lcc',
            lat_1=50.,lon_0=-107.,ax=ax1)

m.drawcountries()
m.drawstates()
m.drawlsmask(land_color='0.7', ocean_color='white', lakes=True)

# Draw latitude/longitude lines every 30 degrees
m.drawmeridians(np.arange(0, 360, 30))
m.drawparallels(np.arange(-90, 90, 30))
x, y = m(x, y)

# Define wind vectors plotted on projection grid
u_grid, new_lons = shiftgrid(180., u, lons, start=False)
v_grid, new_lons = shiftgrid(180., v, lons, start=False)

# Rotate/Interpolate wind from lat/long grid to the map projection grid
u_proj, v_proj, x_proj, y_proj = m.transform_vector(u_grid, v_grid, new_lons,
                                                    lats, 41, 41, returnxy=True)

# Plot Contours
wind_magnitude = np.sqrt(u**2 + v**2)

if plot_contours is True:
    if plot_variable == 'wind':
        figure_plot = wind_magnitude
        figure_contours = wind_contours
        color = 'b'
        linewidth = 0.5
    elif plot_variable == 'temperature':
        figure_plot = temperature
        figure_contours = temperature_contours
        color='r'
        linewidth = 0.7
    else:
        figure_plot = pressure
        figure_contours = pressure_contours
        color = 'k'
        linewidth = 1.5
    contours = m.contour(x, y, figure_plot, figure_contours, colors=color,
                        linestyles='solid', linewidths=linewidth)
    plt.clabel(contours, inline=1, fontsize=10, fmt='%i')

# Plot Wind Barbs
if plot_barbs is True:
    barbs = m.barbs(x_proj, y_proj, u_proj, v_proj, length=5,
                    barbcolor='k', flagcolor='r', linewidth=0.5)

# Generate plot
ax1.set_title(figure_title)
save_name = "{}.png".format(output_filename)
plt.savefig(save_name, bbox_inches='tight')
plt.show()
